import { AdminModule } from '@adminjs/nestjs';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Database, Resource } from '@adminjs/mongoose'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CONFIG_ADMINMODULE } from './config/admin-module.config';
import AdminJS from 'adminjs';

AdminJS.registerAdapter({ Database, Resource })

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/test-adminjs'),
    AdminModule.createAdminAsync(CONFIG_ADMINMODULE),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
