import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Cat, CatDocument } from './schemas/cat.schema';


@Injectable()
export class CatsService {
    constructor(
        @InjectModel(Cat.name) private readonly catModel: Model<CatDocument>
    ){}

    async updateOne(params){
        const gato =  await this.catModel.findByIdAndUpdate(params.id, {name: params.name, age: params.age})
    }
}
