import AdminJS from 'adminjs';
import { Model } from "mongoose";
import { CatDocument } from "../schemas/cat.schema";
const { bundle } = AdminJS;

export const catResource = (catModel: Model<CatDocument>) => ({
    resource: catModel,
    options: {
      properties: {
        owner: {
          type: 'reference',
          reference: 'WefishUser'
        },
        photo: {
          position: -1,
          isVisible: {
            edit: false,
            new: false,
            list: true,
            show: true
          },
          components: { 
            list: bundle('../../components/imagen-listado'),
            show: bundle('../../components/imagen-listado'),
          }
        },
        picture: {
          isVisible: {
            edit: true,
            new: true,
            list: false,
            show: false
          },
          components: {
            edit: bundle('../../components/upload-file')
          }
        }
      },
      actions: {
        edit: {
          before: async (request) => {
            if(request.payload.picture && request.payload.picture != 0) {
              request.payload.picture = request.payload.picture.name;
            }
            if(request.payload.picture == 0) {
              request.payload.picture = "default_specie.jpeg";
            }
            return request;
          }
        },
        getAtlasSpecies: {
          actionType: 'record',
          label: 'Obtener espcies Atlas',
          icon: 'Terminal',
          //guard: 'You can setup guards before an action - just in case.',
          component: bundle('../../components/custom-edit-action'),
          handler: async (request, response, data) => {
            return {
              record: data.record.toJSON()
            }
          }
        }
      }
    }
  });