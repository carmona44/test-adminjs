import { Body, Controller, Post } from '@nestjs/common';
import { CatsService } from './cats.service';

@Controller('cats')
export class CatsController {
    constructor(
        private readonly catsService: CatsService
    ){}

    @Post('update-one')
    updateOne (@Body() params){
        this.catsService.updateOne(params);
        
    }

}
