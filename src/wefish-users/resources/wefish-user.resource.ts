import { Model } from "mongoose";
import { WefishUserDocument } from "../schemas/wefish-user.schema";

export const wefishUserResource = (wefishUserModel: Model<WefishUserDocument>) => ({
    resource: wefishUserModel,
    options: {
      properties: {
        username: {
          isTitle: true
        }
      }
    }
  });