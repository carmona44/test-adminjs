import React, { useState } from 'react';
import { Box, DropZone, Label, DropZoneItem } from '@adminjs/design-system';

const UploadFile = (props) => {
  const { property, record, onChange } = props;
  const [filesToUpload, setFilesToUpload] = useState(false);

  const onUpload = (files) => {
    setFilesToUpload(true);
    const newRecord = {...record};
    const file = files.length && files[0];

    onChange({
      ...newRecord,
      params: {
        ...newRecord.params,
        [property.name]: file,
      }
    });
  }

  const onRemove = () => {
    setFilesToUpload(true);
    const newRecord = {...record};
    const file = 0;

    onChange({
      ...newRecord,
      params: {
        ...newRecord.params,
        [property.name]: file,
      }
    });
  };

  return (
    <Box>
      <Label>{property.label}</Label>
      <DropZone onChange={onUpload} />
      {record.params.picture && !filesToUpload ? (
        <DropZoneItem filename={record.params.picture} src={`https://d1kdty7lddhze2.cloudfront.net/common/species/${record.params.picture}`} onRemove={onRemove} />
      ) : ''}
    </Box>
  )
}

export default UploadFile;