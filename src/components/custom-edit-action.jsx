import React, { useState, useEffect} from 'react';
import { Box, H3,   Table,
  TableRow,
  TableCell,
  TableCaption,
  TableHead,
  TableBody, CardTitle } from '@adminjs/design-system';
import { useNotice, withNotice } from 'adminjs'
import { Input, Label, Button, Modal, Icon, Link, CheckBox } from '@adminjs/design-system'

const MyNewAction = (props) => {
  const MY_MESSAGE = {
    message: 'I am toast message',
    type: 'success',
  }

  const sendNotice = useNotice()
  const [isVisible, setIsVisible] = useState(false)
  const [name, setName] = useState('')
  const [age, setAge] = useState('')
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [atlas, setAtlas] = useState([]);  
  const { resource, action, record, addNotice } = props; 

  const handleChangeName = (event) => {
    setName(event.target.value)
  }

  const handleChangeAge = (event) => {
    setAge(event.target.value)
  }

  function tableItems(atlas) {
    const listItems = atlas?.items?.map((item) =>
      // Correcto! La key debería ser especificada dentro del array.
      <TableRow key={item._id.toString()}>
      <TableCell>
        {item.family}
      </TableCell>
      </TableRow>
    );
    return (
      <Table>
        <TableCaption>
          <CardTitle>
            Especies del atlas
          </CardTitle>
        </TableCaption>
        {listItems}
      </Table>
    );
  }

  const handleSubmit = (event) => {
    setIsVisible(false)
    fetch('http://localhost:3000/cats/update-one', {
        method: 'POST',
          // We convert the React state to JSON and send it as the POST body
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify({name, age, id:record.params._id})
      }).then(function(response) {
        return response
      });
    location.reload()
  }

  const modalProps = {
    title: 'Los cambios se han realizado con éxito',
    variant: 'default',
    icon: 'Checkmark',
    buttons: [
      {
        label: 'Aceptar',
        type: 'submit',
        onClick: handleSubmit
      }
    ],
    onOverlayClick: () => setIsVisible(false),
    onClose: () => setIsVisible(false),
  }

  useEffect(() => {
    let mounted = true
    
    fetch("https://dev.wefish.app/api-meteor/v1/atlas-species")
      .then(res => res.json())
      .then(
        (result) => {
          if (mounted){
            setIsLoaded(true);
            setAtlas(result);
          }
        },
        // Nota: es importante manejar errores aquí y no en 
        // un bloque catch() para que no interceptemos errores
        // de errores reales en los componentes.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
      return () => {
        mounted = false
      };
  }, [])

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <Box flex>
      <Box variant="white" width={1/2} boxShadow="card" mr="xxl" flexShrink={0}>
        <H3>Atlas Species</H3>
        <p>Aquí vas a ver varias especies del Atlas de Wefish</p>
        <p>y un gato ⬇️:</p>
        <br/>
        <p>Nombre: {record.params.name}</p>
        <p>Raza: {record.params.breed}</p>
        <p>Edad: {record.params.age}</p>
        <p>Perro amigo: {record.params.friend}</p>
        <p>Dueño: {record.params.owner}</p>
        <br/>
        <p><img src="https://i.redd.it/rd39yuiy9ns21.jpg" alt="stupid cat" width={300} /></p>
        <form>
          <Box mb="lg" width={1} mt="xl">
            <Label>Nombre:</Label>
            <Input
              placeholder="Escriba el nombre..."
              width="0,5"
              type="text"
              variant="default"
              onChange={handleChangeName}
            />
          </Box>
          <Box mb="lg" width={1}>
            <Input
              placeholder="Escriba la edad..."
              width="0,5"
              id="newAge"
              type="text"
              variant="default"
              onChange={handleChangeAge}
            />  
          </Box>
          <Box>
            <Button onClick={() => addNotice(MY_MESSAGE)}>withNotice</Button>
          </Box>
        </form>
        <Box>
            <Button onClick={() => sendNotice({ message: 'I am awesome' })}>Botón de prueba</Button>
        </Box>
        <Box>
            <Button onClick={() => setIsVisible(true)}>Submit</Button>
        </Box>
        <Box>
            {isVisible && <Modal {...modalProps} />}
        </Box>
      </Box>
      <Box width={1/2} mt="xxl">
        {tableItems(atlas)}
      </Box>
    </Box>
    )
  }

}

export default withNotice(MyNewAction);