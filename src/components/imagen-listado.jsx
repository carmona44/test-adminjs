import React from 'react';

const MyNewAction = (props) => {
  return (
    <img src={`https://d1kdty7lddhze2.cloudfront.net/common/species/${props.record.params.picture}`} alt="cdn image" width={75} />
  )
}

export default MyNewAction;