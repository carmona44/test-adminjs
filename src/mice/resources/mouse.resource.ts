import { Model } from "mongoose";
import { MouseDocument } from "../schemas/mouse.schema";

export const mouseResource = (mouseModel: Model<MouseDocument>) => ({
    resource: mouseModel,
});