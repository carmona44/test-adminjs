import { getModelToken } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CatsModule } from "../cats/cats.module";
import { catResource } from "../cats/resources/cat.resource";
import { CatDocument } from "../cats/schemas/cat.schema";
import { DogsModule } from "../dogs/dogs.module";
import { dogResource } from "../dogs/resources/dog.resource";
import { DogDocument } from "../dogs/schemas/dog.schema";
import { MiceModule } from "../mice/mice.module";
import { mouseResource } from "../mice/resources/mouse.resource";
import { MouseDocument } from "../mice/schemas/mouse.schema";
import { userResource } from "../users/resources/user.resource";
import { UserDocument } from "../users/schemas/user.schema";
import { UsersModule } from "../users/users.module";
import { wefishUserResource } from "../wefish-users/resources/wefish-user.resource";
import { WefishUserDocument } from "../wefish-users/schemas/wefish-user.schema";
import { WefishUsersModule } from "../wefish-users/wefish-users.module";
import * as LANGUAGE_ES from '../locales/es.json';
const bcrypt = require('bcrypt');

export const CONFIG_ADMINMODULE = {
    imports: [
      CatsModule, // importing module that exported model we want to inject
      DogsModule, // importing module that exported model we want to inject
      MiceModule, // importing module that exported model we want to inject
      UsersModule, // importing module that exported model we want to inject
      WefishUsersModule, // importing module that exported model we want to inject
    ],
    inject: [
      getModelToken('Cat'), // using mongoose function to inject dependency
      getModelToken('Dog'), // using mongoose function to inject dependency
      getModelToken('Mouse'), // using mongoose function to inject dependency
      getModelToken('User'), // using mongoose function to inject dependency
      getModelToken('WefishUser'), // using mongoose function to inject dependency
    ],
    useFactory: (
        catModel: Model<CatDocument>, 
        dogModel: Model<DogDocument>, 
        mouseModel: Model<MouseDocument>, 
        userModel: Model<UserDocument>, 
        wefishUserModel: Model<WefishUserDocument>) => ({ // injected dependecy will appear as an argument
      adminJsOptions: {
        locale: LANGUAGE_ES,
        rootPath: '/admin',
        resources: [
          catResource(catModel),
          dogResource(dogModel),
          mouseResource(mouseModel),
          userResource(userModel),
          wefishUserResource(wefishUserModel)
        ],
        branding: {
          companyName: 'WeFish Admin Panel',
          softwareBrothers: false,
          favicon: 'https://cdn.wefish.app/wp-content/uploads/2020/03/cropped-fav-icon-270x270.jpg',
          logo: 'https://cdn.wefish.app/wp-content/uploads/2021/05/cropped-logo-header-1.png'
        }
      },
      auth: {
        authenticate: async (email, password) => {
          const user = await userModel.findOne({ email });
          if (user) {
            const matched = await bcrypt.compare(password, user.encryptedPassword);
            if (matched) {
              return user;
            }
          }
        },
        cookieName: 'test',
        cookiePassword: 'testPass'
      },
    }),
};
