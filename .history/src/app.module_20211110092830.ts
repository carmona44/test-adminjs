import { AdminModule } from '@adminjs/nestjs';
import { Module } from '@nestjs/common';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import AdminJS from 'adminjs';
const bcrypt = require('bcrypt');
import { Database, Resource } from '@adminjs/mongoose'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';
import { Model } from 'mongoose';
import { Cat, CatDocument } from './cats/schemas/cat.schema';
import { Dog, DogDocument } from './cats/schemas/dog.schema';
import { MouseDocument } from './cats/schemas/mouse.schema';
import { UserDocument } from './cats/schemas/user.schema';
import { WefishUserDocument } from './cats/schemas/wefish-user.schema';
const { bundle } = AdminJS

AdminJS.registerAdapter({ Database, Resource })

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/test-adminjs'),
    AdminModule.createAdminAsync({
      imports: [
        CatsModule, // importing module that exported model we want to inject
      ],
      inject: [
        getModelToken('Cat'), // using mongoose function to inject dependency
        getModelToken('Dog'), // using mongoose function to inject dependency
        getModelToken('Mouse'), // using mongoose function to inject dependency
        getModelToken('User'), // using mongoose function to inject dependency
        getModelToken('WefishUser'), // using mongoose function to inject dependency
      ],
      useFactory: (catModel: Model<CatDocument>, dogModel: Model<DogDocument>, mouseModel: Model<MouseDocument>, userModel: Model<UserDocument>, wefishUserModel: Model<WefishUserDocument>) => ({ // injected dependecy will appear as an argument
        adminJsOptions: {
          locale,
          rootPath: '/admin',
          resources: [
            catResource(catModel),
            dogResource(dogModel),
            { resource: mouseModel },
            userResource(userModel),
            wefishUserResource(wefishUserModel)
          ],
          branding: {
            companyName: 'WeFish Admin Panel',
            softwareBrothers: false,
            favicon: 'https://cdn.wefish.app/wp-content/uploads/2020/03/cropped-fav-icon-270x270.jpg',
            logo: 'https://cdn.wefish.app/wp-content/uploads/2021/05/cropped-logo-header-1.png'
          }
        },
        auth: {
          authenticate: async (email, password) => {
            const user = await userModel.findOne({ email });
            if (user) {
              const matched = await bcrypt.compare(password, user.encryptedPassword);
              if (matched) {
                return user;
              }
            }
          },
          cookieName: 'test',
          cookiePassword: 'testPass',
        },
      }),
    }),
    CatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}


const dogResource = (dogModel: Model<DogDocument>) => ({
  resource: dogModel,
  /*options: {
    properties: {
      name: {
        isTitle: false
      },
      age: {
        isTitle: true
      }
    }
  }*/
})

const wefishUserResource = (wefishUserModel: Model<WefishUserDocument>) => ({
  resource: wefishUserModel,
  options: {
    properties: {
      username: {
        isTitle: true
      }
    }
  }
})

const catResource = (catModel: Model<CatDocument>) => ({
  resource: catModel,
  options: {
    properties: {
      owner: {
        type: 'reference',
        reference: 'WefishUser'
      }
    },
    actions: {
      getQuizQuestions: {
        actionType: 'record',
        label: 'Obtener preguntas quizGame',
        icon: 'Terminal',
        //guard: 'You can setup guards before an action - just in case.',
        component: bundle('./components/custom-edit-action'),
        handler: async (request, response, data) => {
          return {
            record: data.record.toJSON()
          }
        }
      }
    }
  }
})

const userResource = (userModel: Model<UserDocument>) => ({
  resource: userModel,
  options: {
    properties: {
      encryptedPassword: {
        isVisible: false,
      },
      password: {
        type: 'string',
        isVisible: {
          list: false, edit: true, filter: false, show: false,
        },
      },
    },
    actions: {
      new: {
        before: async (request) => {
          if(request.payload.password) {
            request.payload = {
              ...request.payload,
              encryptedPassword: await bcrypt.hash(request.payload.password, 10),
              password: undefined,
            }
          }
          return request
        },
        isAccessible: ({ currentAdmin }) => currentAdmin && currentAdmin.role === 'admin',
      },
      edit: {
        isAccessible: ({ currentAdmin }) => currentAdmin && currentAdmin.role === 'admin',
      },
      delete: {
        isAccessible: ({ currentAdmin }) => currentAdmin && currentAdmin.role === 'admin',
      }
    }
  }
});



const locale = {
  language: 'es',
  translations: {
    actions: {
      new: 'Nuevo',
    },
    labels: {
      Cat: 'Gatos'
    },
    resources: {
      Cat: {
        properties: {
          name: 'Nombre',
        },
        actions: {
          new: 'Nuevo gato'
        }
      }
    }
  }
}