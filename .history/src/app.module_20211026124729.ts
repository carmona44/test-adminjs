import { AdminModule } from '@adminjs/nestjs';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    AdminModule.createAdmin({
      adminJsOptions: {
        rootPath: '/admin',
        resources: [],
      }
    }),
    MongooseModule.forRoot('mongodb://localhost/test-adminjs'),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
