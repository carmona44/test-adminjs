import { AdminModule } from '@adminjs/nestjs';
import { Module } from '@nestjs/common';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import AdminJS from 'adminjs';
import { Database, Resource } from '@adminjs/mongoose'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';
import { Model } from 'mongoose';
import { Cat } from './cats/schemas/cat.schema';
import { Dog } from './cats/schemas/dog.schema';
import { MouseDocument } from './cats/schemas/mouse.schema';

AdminJS.registerAdapter({ Database, Resource })

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/test-adminjs'),
    AdminModule.createAdminAsync({
      imports: [
        CatsModule, // importing module that exported model we want to inject
      ],
      inject: [
        getModelToken('Cat'), // using mongoose function to inject dependency
        getModelToken('Dog'), // using mongoose function to inject dependency
        getModelToken('Mouse'), // using mongoose function to inject dependency
      ],
      useFactory: (catModel: Model<Cat>, dogModel: Model<Dog>, mouseModel: Model<MouseDocument>) => ({ // injected dependecy will appear as an argument
        adminJsOptions: {
          rootPath: '/admin',
          resources: [
            { resource: catModel },
            { resource: dogModel },
            { resource: mouseModel },
          ],
          branding: {
            companyName: 'WeFish Admin Panel',
            softwareBrothers: false,
            favicon: 'https://cdn.wefish.app/wp-content/uploads/2020/03/cropped-fav-icon-270x270.jpg',
            logo: 'https://cdn.wefish.app/wp-content/uploads/2021/05/cropped-logo-header-1.png'
          }
        },
      }),
    }),
    CatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
