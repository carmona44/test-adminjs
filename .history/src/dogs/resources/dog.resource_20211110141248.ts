import { Model } from "mongoose";
import { DogDocument } from "../schemas/dog.schema";

export const dogResource = (dogModel: Model<DogDocument>) => ({
    resource: dogModel,
  });