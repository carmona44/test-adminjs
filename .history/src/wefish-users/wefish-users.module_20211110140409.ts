import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { WefishUser, WefishUserSchema } from './schemas/wefish-user.schema';

@Module({
    imports: [
      MongooseModule.forFeature([{ name: WefishUser.name, schema: WefishUserSchema }]),
    ],
    exports: [MongooseModule],
  })
export class WefishUsersModule {}
