import React from 'react';
import { Illustration } from '@adminjs/design-system';

const MyNewAction = (props) => {
  return (
    <Illustration variant="Rocket" />
  )
}

export default MyNewAction;