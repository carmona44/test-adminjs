import React, { useState } from 'react';
import { Box, H3, RecordsTable } from '@adminjs/design-system';
import { useRecords } from 'adminjs'

const MyNewAction = (props) => {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const { resource, action, record } = props;
  const {
    records,
    loading,
    direction,
    sortBy,
    page,
    total,
    fetchData,
    perPage,
  } = useRecords('Cat');


  const handleActionPerformed = () => {
    // here is a trigger for a case when user performs an action without component (like `delete`)
  }

  // these functions goes from useSelectedRecords hook, but are optional.
  const handleSelect = () => {}
  const handleSelectAll = () => {}
  const selectedRecords = () => {}

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <Box flex>
      <Box variant="white" width={1/2} boxShadow="card" mr="xxl" flexShrink={0}>
        <H3>Atlas Species</H3>
        <p>Aquí vas a ver varias especies del Atlas de Wefish</p>
        <p>y un gato ⬇️:</p>
        <br/>
        <p>Nombre: {record.params.name}</p>
        <p>Raza: {record.params.breed}</p>
        <p>Edad: {record.params.age}</p>
        <p>Perro amigo: {record.params.friend}</p>
        <p>Dueño: {record.params.owner}</p>
        <br/>
        <p><img src="https://i.redd.it/rd39yuiy9ns21.jpg" alt="stupid cat" width={300} /></p>
      </Box>
      <Box>
        <RecordsTable
          resource={resource}
          records={records}
          actionPerformed={handleActionPerformed}
          onSelect={handleSelect}
          onSelectAll={handleSelectAll}
          selectedRecords={selectedRecords}
          direction={direction}
          sortBy={sortBy}
          isLoading={loading}
        />
      </Box>
      </Box>
    )
  }

}

export default MyNewAction;