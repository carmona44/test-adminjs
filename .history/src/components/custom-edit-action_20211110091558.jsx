import React from 'react';
import { Box, H3 } from '@adminjs/design-system';

const MyNewAction = (props) => {
  const { resource, action, record } = props;
  // do something with the props and render action
  return (
    <Box flex>
    <Box variant="white" width={1/2} boxShadow="card" mr="xxl" flexShrink={0}>
      <H3>Preguntas QuizGame</H3>
      <p>Aquí vas a ver 10 preguntas del QuizGame</p>
      <p>y un gato ⬇️:</p>
      <p><img src="https://i.redd.it/rd39yuiy9ns21.jpg" alt="stupid cat" width={300} /></p>
    </Box>
    <Box>
      <p>Or (more likely), operate on a returned record:</p>
      <Box overflowX="auto">
        {JSON.stringify(record)}
      </Box>
    </Box>
    </Box>
  )
}

export default MyNewAction;