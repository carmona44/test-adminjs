import React, { useState, useEffect } from 'react';
import { Box, H3 } from '@adminjs/design-system';

const MyNewAction = (props) => {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const { resource, action, record } = props;

  useEffect(() => {
    fetch("https://api.example.com/items")
      .then(res => res.json())
      .then(
        (result) => {
          console.log('Entro');
          setIsLoaded(true);
          setItems(result);
        },
        // Nota: es importante manejar errores aquí y no en 
        // un bloque catch() para que no interceptemos errores
        // de errores reales en los componentes.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [])

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <Box flex>
      <Box variant="white" width={1/2} boxShadow="card" mr="xxl" flexShrink={0}>
        <H3>Preguntas QuizGame</H3>
        <p>Aquí vas a ver 10 preguntas del QuizGame</p>
        <p>y un gato ⬇️:</p>
        <br/>
        <p>Nombre: {record.params.name}</p>
        <p>Raza: {record.params.breed}</p>
        <p>Edad: {record.params.age}</p>
        <p>Perro amigo: {record.params.friend}</p>
        <p>Dueño: {record.params.owner}</p>
        <br/>
        <p><img src="https://i.redd.it/rd39yuiy9ns21.jpg" alt="stupid cat" width={300} /></p>
      </Box>
      <Box>
        <Box overflowX="auto">
          {JSON.stringify(record)}
        </Box>
      </Box>
      </Box>
    )
  }

}

export default MyNewAction;