import React from 'react';
import { Box, Label } from 'adminjs';

const MyNewAction = (props) => {
  const { resource, action, record } = props;
  // do something with the props and render action
  return (
    <div>
      <Box>Some Action Content</Box>
      <Label>Some styled text</Label>
    </div>
  )
}

export default MyNewAction;