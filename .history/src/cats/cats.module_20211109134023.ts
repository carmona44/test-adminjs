import { Module } from '@nestjs/common';
import { CatsService } from './cats.service';
import { CatsController } from './cats.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Cat, CatSchema } from './schemas/cat.schema';
import { Dog, DogSchema } from './schemas/dog.schema';
import { Mouse, MouseSchema } from './schemas/mouse.schema';
import { User, UserSchema } from './schemas/user.schema';
import { WefishUser, WefishUserSchema } from './schemas/wefish-user.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Cat.name, schema: CatSchema }]),
    MongooseModule.forFeature([{ name: Dog.name, schema: DogSchema }]),
    MongooseModule.forFeature([{ name: Mouse.name, schema: MouseSchema }]),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MongooseModule.forFeature([{ name: WefishUser.name, schema: WefishUserSchema }]),
  ],
  controllers: [CatsController],
  providers: [CatsService],
  exports: [MongooseModule],
})
export class CatsModule {}
