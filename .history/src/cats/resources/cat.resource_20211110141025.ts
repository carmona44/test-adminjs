const { bundle } = AdminJS;
import { Model } from "mongoose";
import { CatDocument } from "../schemas/cat.schema";

export const catResource = (catModel: Model<CatDocument>) => ({
    resource: catModel,
    options: {
      properties: {
        owner: {
          type: 'reference',
          reference: 'WefishUser'
        },
        photo: {
          position: -1,
          components: { list: bundle('../../components/imagen-listado')}
        }
      },
      actions: {
        getAtlasSpecies: {
          actionType: 'record',
          label: 'Obtener espcies Atlas',
          icon: 'Terminal',
          //guard: 'You can setup guards before an action - just in case.',
          component: bundle('../../components/custom-edit-action'),
          handler: async (request, response, data) => {
            return {
              record: data.record.toJSON()
            }
          }
        }
      }
    }
  });