import { Module } from '@nestjs/common';
import { CatsService } from './cats.service';
import { CatsController } from './cats.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Cat, CatSchema } from './schemas/cat.schema';
import { Dog, DogSchema } from './schemas/dog.schema';
import { Mouse, MouseSchema } from './schemas/mouse.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Cat.name, schema: CatSchema }]),
    MongooseModule.forFeature([{ name: Dog.name, schema: DogSchema }]),
    MongooseModule.forFeature([{ name: Mouse.name, schema: MouseSchema }]),
  ],
  controllers: [CatsController],
  providers: [CatsService],
  exports: [MongooseModule],
})
export class CatsModule {}
