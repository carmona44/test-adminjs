import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Cat, CatSchema } from './schemas/cat.schema';
import { Mouse, MouseSchema } from './schemas/mouse.schema';
import { User, UserSchema } from './schemas/user.schema';
import { WefishUser, WefishUserSchema } from './schemas/wefish-user.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Cat.name, schema: CatSchema }]),
    MongooseModule.forFeature([{ name: Mouse.name, schema: MouseSchema }]),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MongooseModule.forFeature([{ name: WefishUser.name, schema: WefishUserSchema }]),
  ],
  controllers: [],
  providers: [],
  exports: [MongooseModule],
})
export class CatsModule {}
