import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Dog } from './dog.schema';
import { WefishUser } from './wefish-user.schema';

export type CatDocument = Cat & Document;

@Schema()
export class Cat {
  @Prop()
  name: string;

  @Prop()
  age: number;

  @Prop()
  breed: string;

  @Prop({type: MongooseSchema.Types.ObjectId, ref: "Dog"})
  friend: Dog;

  @Prop()
  owner: string;
}

export const CatSchema = SchemaFactory.createForClass(Cat);

CatSchema.virtual('_owner', {
  ref: 'WefishUser', // The model to use
  localField: 'owner', // Find people where `localField`
  foreignField: '_id',
});