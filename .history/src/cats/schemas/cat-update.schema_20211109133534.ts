import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Dog } from './dog.schema';

export type CatUpdateDocument = CatUpdate & Document;

@Schema()
export class CatUpdate {
  @Prop()
  name: string;

  @Prop()
  age: number;

  @Prop()
  breed: string;

  @Prop({type: MongooseSchema.Types.ObjectId, ref: "Dog"})
  friend: Dog;

  @Prop({type: MongooseSchema.Types.String})
  owner: string;
}

export const CatUpdateSchema = SchemaFactory.createForClass(CatUpdate);