import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Dog } from './dog.schema';

export type CatDocument = Cat & Document;

@Schema()
export class Cat {
  @Prop()
  name: string;

  @Prop()
  age: number;

  @Prop()
  breed: string;

  @Prop({type: Types.ObjectId, ref: "Dog"})
  friend: Dog;
}

export const CatSchema = SchemaFactory.createForClass(Cat);