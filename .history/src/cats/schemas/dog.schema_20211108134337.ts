import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type DogDocument = Dog & Document;

@Schema()
export class Dog {
  @Prop()
  email: string;

  @Prop()
  age: number;

  @Prop()
  hairStyle: string;
}

export const DogSchema = SchemaFactory.createForClass(Dog);