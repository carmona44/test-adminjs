import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type MouseDocument = Mouse & Document;

@Schema()
export class Mouse {
  @Prop()
  name: string;

  @Prop()
  age: number;

  @Prop()
  height: number;

  @Prop()
  color: string;
  
  @Prop()
  breed: string;

  @Prop()
  isFamous: boolean;
}

export const MouseSchema = SchemaFactory.createForClass(Mouse);