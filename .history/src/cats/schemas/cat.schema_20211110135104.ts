import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Dog } from '../../dogs/schemas/dog.schema';

export type CatDocument = Cat & Document;

@Schema()
export class Cat {
  @Prop()
  name: string;

  @Prop()
  age: number;

  @Prop()
  breed: string;

  @Prop({type: MongooseSchema.Types.ObjectId, ref: "Dog"})
  friend: Dog;

  @Prop({type: MongooseSchema.Types.String, ref: "WefishUser"})
  owner: string;

  /*@Prop()
  picture: string;*/
}

export const CatSchema = SchemaFactory.createForClass(Cat);