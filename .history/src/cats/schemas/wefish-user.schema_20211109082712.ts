import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type WefishUserDocument = WefishUser & Document;

@Schema()
export class WefishUser {
  @Prop({ required: true })
  username: string;

  @Prop()
  createdAt: Date;  

  @Prop()
  sessions: number;  
}

export const WefishUserSchema = SchemaFactory.createForClass(WefishUser);