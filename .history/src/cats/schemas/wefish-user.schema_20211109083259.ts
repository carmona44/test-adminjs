import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type WefishUserDocument = WefishUser & Document;

@Schema({ collection: 'wefish-users' })
export class WefishUser {
  @Prop()
  _id: string;

  @Prop({ required: true })
  username: string;

  @Prop()
  createdAt: Date;  

  @Prop()
  sessions: number;  
}

export const WefishUserSchema = SchemaFactory.createForClass(WefishUser);