import { AdminModule } from '@adminjs/nestjs';
import { Module } from '@nestjs/common';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import AdminJS from 'adminjs';
const bcrypt = require('bcrypt');
import { Database, Resource } from '@adminjs/mongoose'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';
import { Model } from 'mongoose';
import { CatDocument } from './cats/schemas/cat.schema';
import { DogDocument } from './dogs/schemas/dog.schema';
import { MouseDocument } from './mice/schemas/mouse.schema';
import { UserDocument } from './users/schemas/user.schema';
import { WefishUserDocument } from './wefish-users/schemas/wefish-user.schema';
import { DogsModule } from './dogs/dogs.module';
import { MiceModule } from './mice/mice.module';
import { UsersModule } from './users/users.module';
import { WefishUsersModule } from './wefish-users/wefish-users.module';
import { catResource } from './cats/resources/cat.resource';
import { dogResource } from './dogs/resources/dog.resource';
import { mouseResource } from './mice/resources/mouse.resource';
const { bundle } = AdminJS

AdminJS.registerAdapter({ Database, Resource })

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/test-adminjs'),
    AdminModule.createAdminAsync({
      imports: [
        CatsModule, // importing module that exported model we want to inject
        DogsModule, // importing module that exported model we want to inject
        MiceModule, // importing module that exported model we want to inject
        UsersModule, // importing module that exported model we want to inject
        WefishUsersModule, // importing module that exported model we want to inject
      ],
      inject: [
        getModelToken('Cat'), // using mongoose function to inject dependency
        getModelToken('Dog'), // using mongoose function to inject dependency
        getModelToken('Mouse'), // using mongoose function to inject dependency
        getModelToken('User'), // using mongoose function to inject dependency
        getModelToken('WefishUser'), // using mongoose function to inject dependency
      ],
      useFactory: (catModel: Model<CatDocument>, dogModel: Model<DogDocument>, mouseModel: Model<MouseDocument>, userModel: Model<UserDocument>, wefishUserModel: Model<WefishUserDocument>) => ({ // injected dependecy will appear as an argument
        adminJsOptions: {
          locale,
          rootPath: '/admin',
          resources: [
            catResource(catModel),
            dogResource(dogModel),
            mouseResource(mouseModel),
            userResource(userModel),
            wefishUserResource(wefishUserModel)
          ],
          branding: {
            companyName: 'WeFish Admin Panel',
            softwareBrothers: false,
            favicon: 'https://cdn.wefish.app/wp-content/uploads/2020/03/cropped-fav-icon-270x270.jpg',
            logo: 'https://cdn.wefish.app/wp-content/uploads/2021/05/cropped-logo-header-1.png'
          }
        },
        auth: {
          authenticate: async (email, password) => {
            const user = await userModel.findOne({ email });
            if (user) {
              const matched = await bcrypt.compare(password, user.encryptedPassword);
              if (matched) {
                return user;
              }
            }
          },
          cookieName: 'test',
          cookiePassword: 'testPass',
        },
      }),
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

const wefishUserResource = (wefishUserModel: Model<WefishUserDocument>) => ({
  resource: wefishUserModel,
  options: {
    properties: {
      username: {
        isTitle: true
      }
    }
  }
})

const userResource = (userModel: Model<UserDocument>) => ({
  resource: userModel,
  options: {
    properties: {
      encryptedPassword: {
        isVisible: false,
      },
      password: {
        type: 'string',
        isVisible: {
          list: false, edit: true, filter: false, show: false,
        },
      },
    },
    actions: {
      new: {
        before: async (request) => {
          if(request.payload.password) {
            request.payload = {
              ...request.payload,
              encryptedPassword: await bcrypt.hash(request.payload.password, 10),
              password: undefined,
            }
          }
          return request
        },
        isAccessible: ({ currentAdmin }) => currentAdmin && currentAdmin.role === 'admin',
      },
      edit: {
        isAccessible: ({ currentAdmin }) => currentAdmin && currentAdmin.role === 'admin',
      },
      delete: {
        isAccessible: ({ currentAdmin }) => currentAdmin && currentAdmin.role === 'admin',
      }
    }
  }
});



const locale = {
  language: 'es',
  translations: {
    actions: {
      new: 'Nuevo',
    },
    labels: {
      Cat: 'Gatos'
    },
    resources: {
      Cat: {
        properties: {
          name: 'Nombre',
        },
        actions: {
          new: 'Nuevo gato'
        }
      }
    }
  }
}