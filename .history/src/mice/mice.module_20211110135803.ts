import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Mouse, MouseSchema } from './mouse.schema';

@Module({
    imports: [
      MongooseModule.forFeature([{ name: Mouse.name, schema: MouseSchema }]),
    ],
    exports: [MongooseModule],
  }
  )
export class MiceModule {}
