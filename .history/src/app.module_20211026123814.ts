import { AdminModule } from '@adminjs/nestjs';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    AdminModule.createAdmin({
      adminJsOptions: {
        rootPath: '/admin',
        resources: [],
      }
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
