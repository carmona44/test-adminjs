import { AdminModule } from '@adminjs/nestjs';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import Adminjs from 'adminjs';
import { Database, Resource } from '@adminjs/mongoose'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';

Adminjs.registerAdapter({ Database, Resource })

@Module({
  imports: [
    AdminModule.createAdmin({
      adminJsOptions: {
        rootPath: '/admin',
        resources: [],
      }
    }),
    MongooseModule.forRoot('mongodb://localhost/test-adminjs'),
    CatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
