import { AdminModule } from '@adminjs/nestjs';
import { Module } from '@nestjs/common';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import AdminJS from 'adminjs';
import { Database, Resource } from '@adminjs/mongoose'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';
import { Model } from 'mongoose';
import { CatDocument } from './cats/schemas/cat.schema';

AdminJS.registerAdapter({ Database, Resource })

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/test-adminjs'),
    AdminModule.createAdminAsync({
      imports: [
        CatsModule, // importing module that exported model we want to inject
      ],
      inject: [
        getModelToken('Cat'), // using mongoose function to inject dependency
      ],
      useFactory: (catModel: Model<CatDocument>) => ({ // injected dependecy will appear as an argument
        adminJsOptions: {
          rootPath: '/admin',
          resources: [
            { resource: catModel },
          ],
        },
      }),
    }),
    CatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
